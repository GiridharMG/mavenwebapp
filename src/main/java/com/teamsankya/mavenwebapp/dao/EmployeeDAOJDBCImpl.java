package com.teamsankya.mavenwebapp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

import com.teamsankya.mavenwebapp.dto.EmployeeBean;

public class EmployeeDAOJDBCImpl implements EmployeeDAO {

	/*private String idGenarstor() {
		char[] idChar = {'a' , 'b', 'c','0', '1','2','3'};
		String id = "";
		Random random = new Random();
		for(int i = 0; i<8; i++) {
			int index = random.nextInt(idChar.length);
			id = id + idChar[index];
		}
		return id;
	}
	private String uniqueId() {
		String id = null;
		EmployeeBean bean = null;
		do {
			bean = null;
			id = idGenarstor();
			bean = getEmployee(id);
		}while(bean==null);
		return null;
	}*/
	public void createEmployee(EmployeeBean bean) {
		//String id = uniqueId();
		String dbUrl = "jdbc:mysql://localhost:3306/employee_db?user=root&password=root";
		String sql1 = "insert into employee_info values(?,?,?)";
		String sql2 = "insert into employee_contact_info values(?,?,?)";
		String sql3 = "insert into employee_address_info values(?,?,?,?,?)";
		System.out.println("Entering try block");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Creating JDBC objects");
			try (Connection con = DriverManager.getConnection(dbUrl);
					PreparedStatement pstmt1 = con.prepareStatement(sql1);
					PreparedStatement pstmt2 = con.prepareStatement(sql2);
					PreparedStatement pstmt3 = con.prepareStatement(sql3);) {
				System.out.println("Setting values for 1st query");
				pstmt1.setInt(1, bean.getEid());
				pstmt1.setString(2, bean.getFname());
				pstmt1.setString(3, bean.getLname());

				System.out.println("Setting values for 2nd query");
				pstmt2.setInt(1, bean.getEid());
				pstmt2.setString(3, bean.getEmail());
				pstmt2.setInt(2, bean.getPhno());

				System.out.println("Setting values for 3rd query");
				pstmt3.setInt(1, bean.getEid());
				pstmt3.setString(2, bean.getAddress1());
				pstmt3.setString(3, bean.getAddress2());
				pstmt3.setString(4, bean.getCity());
				pstmt3.setInt(5, bean.getPincode());

				System.out.println("Executing the queries");
				pstmt1.execute();
				pstmt2.execute();
				pstmt3.execute();

				System.out.println("End of try block");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} // end of try catch block
	}// end of createEmployee method

	@Override
	public EmployeeBean getEmployee(int eid) {
		String dbUrl = "jdbc:mysql://localhost:3306/employee_db?user=root&password=root";
		String sql = "select * from employee_info ei, employee_address_info ea,"
				+ " employee_contact_info ec where ei.eid = ea.eid and" + " ei.eid = ec.eid and ei.eid=?";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Creating JDBC objects");
			try (Connection con = DriverManager.getConnection(dbUrl);
					PreparedStatement pstmt = con.prepareStatement(sql)) {
				pstmt.setInt(1, eid);
				try (ResultSet rs = pstmt.executeQuery()) {
					EmployeeBean bean = new EmployeeBean();
					if (rs.next()) {
						bean.setEid(rs.getInt(1));
						bean.setFname(rs.getString("first_name"));
						bean.setLname(rs.getString("last_name"));
						bean.setEmail(rs.getString("email"));
						bean.setPhno(rs.getInt("ph_no"));
						bean.setAddress1(rs.getString("address1"));
						bean.setAddress2(rs.getString("address2"));
						bean.setCity(rs.getString("city"));
						bean.setPincode(rs.getInt("pincode"));
						return bean;
					} else {
						return null;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}// end of getEmployee method
}// end of EmployeeDAOJDBCImpl
