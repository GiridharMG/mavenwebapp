package com.teamsankya.mavenwebapp.dao;

import com.teamsankya.mavenwebapp.dto.EmployeeBean;

public interface EmployeeDAO {
	public void createEmployee(EmployeeBean bean);
	public EmployeeBean getEmployee(int eid);
}
