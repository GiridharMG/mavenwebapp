package com.teamsankya.mavenwebapp.util;

import com.teamsankya.mavenwebapp.dao.EmployeeDAO;
import com.teamsankya.mavenwebapp.dao.EmployeeDAOHibernateImpl;
import com.teamsankya.mavenwebapp.dao.EmployeeDAOJDBCImpl;

public class EmployeeDAOFactory {
	
	private EmployeeDAOFactory(){}
	
	private static String dbInteraction = "jdbc";
	
	public static EmployeeDAO getEmployeeDAOInstance(){
		
		EmployeeDAO dao = null;
		if(dbInteraction.equals("jdbc")){
			dao = new EmployeeDAOJDBCImpl();
		}else if(dbInteraction.equals("hibernate")){
			dao = new EmployeeDAOHibernateImpl();
		}/*else if(dbInteraction.equals("spring-jdbc")){
			dao = new EmployeeDAOSpringJDBCImpl();
		}*/
		return dao;
	}
	
}//End of Class
