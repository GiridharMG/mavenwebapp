package com.teamsanlya.mavenwebapp.test;

import org.junit.Test;

import junit.framework.TestCase;
import net.sourceforge.jwebunit.WebTester;

public class TestCases extends TestCase {
	private WebTester tester = new WebTester();
	private static final String BASE_URL = "http://localhost:8080/mavenwebapp/";
	@Override
	protected void setUp() throws Exception {
		tester.getTestContext()
				.setBaseUrl(BASE_URL);
	}
	
	@Test
	public void testCase1() {
		tester.beginAt("/index.jsp");
		//    http://localhost:8080/mavenwebapp/index.jsp
		tester.assertFormPresent();
		tester.assertTextPresent("Hello World");
		tester.setFormElement("eid", "1");
		tester.assertTextPresent("Hello World");
		/*tester.setFormElement("fname", "Giridhar");
		tester.setFormElement("lname", "MG");
		tester.setFormElement("email", "mg.giridhar@teamsankya.com");
		tester.setFormElement("phno", "1234567890");
		tester.setFormElement("address1", "xyz");
		tester.setFormElement("address2", "abc");
		tester.setFormElement("city", "bangalore");
		tester.setFormElement("pincode", "123456");
		tester.submit();*/
		//tester.assertTextPresent("Employee data inserted");
		//tester.clickLinkWithText("Click here to insert more employee");
	}
}








